var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var env = require('dotenv').load();

// var routes
var v1_route = require('./api/v1/routes');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// use routes
app.use('/api/v1', v1_route);

module.exports = app;
