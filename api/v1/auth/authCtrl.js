module.exports = {
  register: function(req, res){
    res.json('Auth register');
  },
  login: function(req, res){
    res.json('Auth login');
  },
  reset: function(req, res){
    res.json('Auth reset password');
  },
};
