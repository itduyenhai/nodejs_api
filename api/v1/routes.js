const express = require('express');
const router = express.Router();

// require Controller
const homeCtrl = require('./home/homeCtrl');
const userCtrl = require('./user/userCtrl');
const authCtrl = require('./auth/authCtrl');

/* Home */
router.get('/', homeCtrl.index);

/* User */
router.get('/user', userCtrl.index);

/* Auth */
router.post('/register', authCtrl.register);
router.post('/login', authCtrl.login);

module.exports = router;
