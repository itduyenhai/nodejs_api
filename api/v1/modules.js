module.exports = {
  'user' : {
    'getAll': '/api/v1/user',
    'getID': '/api/v1/user/:id',
    'store' : '/api/v1/user',
    'update' : '/api/v1/user/:id',
    'delete' : '/api/v1/user/:id'
  },
  'home': {
    'index':'/api/v1'
  }
}
